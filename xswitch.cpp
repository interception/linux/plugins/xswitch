#include <regex>
#include <atomic>
#include <chrono>
#include <cstdio>
#include <future>
#include <string>
#include <memory>
#include <thread>
#include <csetjmp>
#include <cstdlib>
#include <utility>
#include <stdexcept>

extern "C" {
#include <unistd.h>
#include <linux/input.h>
#include <linux/limits.h>
#include <X11/Xlib.h>
#include <X11/Xmu/WinUtil.h>
}

#include <boost/interprocess/ipc/message_queue.hpp>

using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using boost::interprocess::open_only;
using boost::interprocess::message_queue;

void print_usage(FILE *stream, const char *program) {
    // clang-format off
    fprintf(stream,
            "xswitch - redirect stdin to a muxer if window matches\n"
            "\n"
            "usage: %s [-h] [-r rate] [-w window] [-a application] [-c class] [-o muxer]\n"
            "\n"
            "options:\n"
            "    -h             show this message and exit\n"
            "    -r rate        rate in milliseconds to check current window (default: 500)\n"
            "    -w window      regex to match window title\n"
            "    -a application regex to match application name\n"
            "    -c class       regex to match application class\n"
            "    -o muxer       name of muxer to switch to\n",
            program);
    // clang-format on
}

struct current_window {
    std::string window_name, application_name, class_name;
};

struct window_filter {
    bool matches(const current_window &window) const {
        if (filter_window_name &&
            !std::regex_match(window.window_name, window_name))
            return false;
        if (filter_application_name &&
            !std::regex_match(window.application_name, application_name))
            return false;
        if (filter_class_name &&
            !std::regex_match(window.class_name, class_name))
            return false;
        return filter_window_name || filter_application_name ||
               filter_class_name;
    }

    void set_window_name(const std::string &window_name_regex) {
        filter_window_name = true;
        window_name.assign(window_name_regex, std::regex::optimize);
    }

    void set_application_name(const std::string &application_name_regex) {
        filter_application_name = true;
        application_name.assign(application_name_regex, std::regex::optimize);
    }

    void set_class_name(const std::string &class_name_regex) {
        filter_class_name = true;
        class_name.assign(class_name_regex, std::regex::optimize);
    }

    bool filter_window_name      = false;
    bool filter_application_name = false;
    bool filter_class_name       = false;
    std::regex window_name, application_name, class_name;
};

std::string get_user_home() {
    FILE *p =
        popen(R"(who | grep -m 1 '(:0)' | awk '{printf "%s", $1;}')", "r");
    if (!p)
        return "";

    char buffer[PATH_MAX], *result;

    buffer[0] = 0;

    result = std::fgets(buffer, sizeof(buffer), p);
    pclose(p);
    if (!result)
        return "";

    if (std::strlen(buffer) == 0)
        return "";

    std::string username = buffer;

    p = popen(("eval echo -n ~" + username).c_str(), "r");
    if (!p)
        return "";

    buffer[0] = 0;

    result = std::fgets(buffer, sizeof(buffer), p);
    pclose(p);
    if (!result)
        return "";

    return buffer;
}

bool get_current_window(current_window &window) {
    window = {};

    // get display of user on :0, if any
    static Display *display = nullptr;
    static std::jmp_buf env;
    static auto x_error_handler = [] {
        if (display) {
            Display *copy = display;
            display       = nullptr;
            XCloseDisplay(copy);
        }
        std::longjmp(env, 1);
    };

    if (setjmp(env))
        return false;

    if (!display) {
        {
            std::string home = get_user_home();
            if (home.empty())
                return false;

            setenv("XAUTHORITY", (home + "/.Xauthority").c_str(), true);
        }

        XSetIOErrorHandler([](Display *) {
            x_error_handler();
            return 0;
        });
        display = XOpenDisplay(":0");
        if (!display)
            return false;

        XSetErrorHandler([](Display *, XErrorEvent *) {
            x_error_handler();
            return 0;
        });
    }

    Window x_window = None;

    // get window with focus
    int revert_to_return;
    XGetInputFocus(display, &x_window, &revert_to_return);
    if (x_window == None)
        return false;

    Window parent = x_window;
    Window root   = None;
    Window *children;
    unsigned int nchildren;

    // get top window
    while (parent != root) {
        x_window = parent;
        if (XQueryTree(display, x_window, &root, &parent, &children,
                       &nchildren))
            XFree(children);
        else
            return false;
    }

    // get named window
    x_window = XmuClientWindow(display, x_window);

    // get window name
    XTextProperty prop;
    if (XGetWMName(display, x_window, &prop)) {
        char **text_list = nullptr;
        int count;
        int result =
            Xutf8TextPropertyToTextList(display, &prop, &text_list, &count);
        if (result == Success || result > 0)
            if (text_list) {
                window.window_name = text_list[0];
                XFreeStringList(text_list);
            }
    } else
        return false;

    // get application name and application class
    XClassHint *class_hint = XAllocClassHint();
    if (!class_hint)
        return false;
    struct defer {
        XClassHint *class_hint;
        ~defer() { XFree(class_hint); }
    } defer{class_hint};

    if (XGetClassHint(display, x_window, class_hint)) {
        window.application_name = class_hint->res_name;
        window.class_name       = class_hint->res_class;
    } else
        return false;

    return true;
}

unsigned long rate = 500;
std::atomic<bool> finish{false};
std::atomic<size_t> current_muxer{0};

int main(int argc, char *argv[]) try {
    std::vector<window_filter> window_filters;
    std::vector<std::vector<std::unique_ptr<message_queue>>> muxers;

    muxers.emplace_back();

    for (int opt, last_opt = 0;
         (opt = getopt(argc, argv, "hr:w:a:c:o:")) != -1;) {
        switch (opt) {
            case 'h':
                return print_usage(stdout, argv[0]), EXIT_SUCCESS;
            case 'r':
                if (last_opt)
                    break;
                rate     = std::stoul(optarg);
                last_opt = 'r';
                continue;
            case 'w':
                if (!last_opt || last_opt == 'r' || last_opt == 'o')
                    window_filters.emplace_back();
                else if (window_filters.back().filter_window_name)
                    break;
                window_filters.back().set_window_name(optarg);
                last_opt = 'w';
                continue;
            case 'a':
                if (!last_opt || last_opt == 'r' || last_opt == 'o')
                    window_filters.emplace_back();
                else if (window_filters.back().filter_application_name)
                    break;
                window_filters.back().set_application_name(optarg);
                last_opt = 'a';
                continue;
            case 'c':
                if (!last_opt || last_opt == 'r' || last_opt == 'o')
                    window_filters.emplace_back();
                else if (window_filters.back().filter_class_name)
                    break;
                window_filters.back().set_class_name(optarg);
                last_opt = 'c';
                continue;
            case 'o':
                if (last_opt == 'w' || last_opt == 'a' || last_opt == 'c')
                    muxers.emplace_back();
                muxers.back().emplace_back(
                    new message_queue(open_only, optarg));
                last_opt = 'o';
                continue;
        }

        return print_usage(stderr, argv[0]), EXIT_FAILURE;
    }

    muxers.emplace_back();

    auto current_window_loop = std::async(
        std::launch::async,
        [](const std::vector<window_filter> &window_filters) {
            try {
                current_window window;
                while (!finish) {
                    size_t current = 0;
                    if (get_current_window(window)) {
                        size_t muxer = 1;
                        for (const auto &window_filter : window_filters) {
                            if (window_filter.matches(window)) {
                                current = muxer;
                                break;
                            }
                            ++muxer;
                        }
                    }
                    current_muxer = current;
                    sleep_for(milliseconds(rate));
                }
            } catch (...) {
                finish = true;
                throw;
            }
        },
        std::move(window_filters));

    struct defer {
        ~defer() { finish = true; }
    } defer;

    std::setbuf(stdin, nullptr);

    input_event input;
    while (!finish)
        if (std::fread(&input, sizeof input, 1, stdin) == 1) {
            size_t current = current_muxer;
            for (auto &muxer : muxers[current])
                if (!muxer->try_send(&input, sizeof input, 0))
                    throw std::runtime_error("outgoing muxer is full, exiting");
        } else if (std::ferror(stdin))
            throw std::runtime_error("error reading input event from stdin");
        else if (std::feof(stdin))
            break;
} catch (const std::exception &e) {
    return std::fprintf(stderr,
                        R"(an exception occurred: "%s")"
                        "\n",
                        e.what()),
           EXIT_FAILURE;
}
