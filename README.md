# xswitch

`xswitch` is an additional tool for [_Interception Tools_][interception-tools]
that works similarly to the [`mux`] tool in switch mode, but that **switch**
_muxers_ based on current [**X**][X] window properties (instead of muxer
activity).

The purpose is to have application specific shortcuts, chording, etc.

## Dependencies

- [Interception Tools][interception-tools]
- [Xlib]

## Build dependencies

- [CMake]
- [Boost.Interprocess][interprocess]

## Building

```
$ git clone https://gitlab.com/interception/linux/plugins/xswitch.git
$ cd xswitch
$ cmake -B build -DCMAKE_BUILD_TYPE=Release
$ cmake --build build
```

## Execution

```
xswitch - redirect stdin to a muxer if window matches

usage: xswitch [-h] [-r rate] [-w window] [-a application] [-c class] [-o muxer]

options:
    -h             show this message and exit
    -r rate        rate in milliseconds to check current window (default: 500)
    -w window      regex to match window title
    -a application regex to match application name
    -c class       regex to match application class
    -o muxer       name of muxer to switch to
```

`xswitch` is an [_Interception Tools_][interception-tools] tool. An example
application:

```yaml
- CMD: mux -c keyboard -c caps2esc
- JOB:
    - mux -i caps2esc | caps2esc | mux -o keyboard
    - mux -i keyboard | uinput -c /etc/interception/keyboard.yaml
- JOB: intercept -g $DEVNODE | xswitch -o keyboard -a Alacritty -o caps2esc
  DEVICE:
    LINK: /dev/input/by-path/platform-i8042-serio-0-event-kbd
```

Which will grab the i8042 keyboard (`intercept -g $DEVNODE`) once it's detected
(`LINK: /dev/input/by-path/platform-i8042-serio-0-event-kbd`), and redirect its
events to _muxer_ `keyboard` by default (`xswitch -o keyboard …`) or to _muxer_
`caps2esc`, if current application name is “Alacritty” (`xswitch … -a Alacritty
-o caps2esc`).

Endpoint at `keyboard` _muxer_ is readily consumed by the virtual keyboard
clone (`mux -i keyboard | uinput -c /etc/interception/keyboard.yaml`), while at
pipeline `caps2esc`, there's a detour for applying `caps2esc` before it goes
for consumption (`mux -i caps2esc | caps2esc | mux -o keyboard`). So `caps2esc`
is either active or not depending upon whether application name is “Alacritty”.

The clone of the i8042 keyboard is created from [previously
stored][device-links] configuration snapshot (`sudo uinput -p -d
/dev/input/by-path/platform-i8042-serio-0-event-kbd | sudo tee
/etc/interception/keyboard.yaml`).

The `xswitch` tool accepts an output (`-o X`), or multiple (`-o Y -o Z`), that
will be redirected to (with duplicated redirection if multiple), when current
window matches the `-w`, `-a`, `-c` flags passed before it (`-a Navigator -c
firefox -o X`). For `-o` passed first without `-w`, `-a`, `-c` coming before,
it's set as default output for non matched windows, which is generally
advisable to have. To sum it up, `-w`, `-a`, `-c` (or a combination) can be
passed multiple times, with many `-o`s following each match combination, to
provide specific outputs for each match.

For docs on [X] window title, application and class you may check:

- [`WM_NAME` Property](https://www.x.org/releases/current/doc/xorg-docs/icccm/icccm.html#WM_NAME_Property)
- [`WM_CLASS` Property](https://www.x.org/releases/current/doc/xorg-docs/icccm/icccm.html#WM_CLASS_Property)
- [Setting and Reading the `WM_CLASS` Property](https://www.x.org/releases/current/doc/libX11/libX11/libX11.html#Setting_and_Reading_the_WM_CLASS_Property)

For obtaining those, you should use the [property displayer for X][xprop] (just
execute `xprop` and click on any window) and look for `WM_NAME(STRING)` and
`WM_CLASS(STRING)`.

For more information about the [_Interception Tools_][interception-tools], check
the project's website.

## Installation

I'm maintaining an Archlinux package on AUR:

- <https://aur.archlinux.org/packages/interception-xswitch>

## Caveats

**This tool is experimental and may cause some issues.**

_Interception Tools_ wasn't created with X in mind and is currently generally
being used through [`udevmon.service`][udevmon] as root, which may be enabled
on boot. So for this tool to actually work (even on boot) it has to leverage
some "hacks" (using `who`, etc) to obtain the current X user, or fallback if it
can't be inferred.

It has to pool for current window, which works, but isn't great. I didn't found
an event based approach.

And finally, careful with conflicting shortcuts/chording meanwhile application
switching is on the fly. Avoid relying on chording schemes that are conflicting
between applications, because given that the plugin pipelines run
independently, they don't hold state of one another, so if you create a key
chord to switch to a window for example, and because of this window switch, a
switch of pipelines happens concurrently, you may get screwed due to broken key
down/up states in each pipeline, like having your input blocked, and have to
grab another keyboard (that won't match in `udevmon.conf`) to restart
`udevmon`, or use `ssh` for that, etc.

Besides care, tuning the window check rate (`-r`) may affect this positively.

Due to that, you may opt not relying on such pipeline switch scheme of
`xswitch`, and instead, just grab parts of its source code for checking current
window, etc, and embed decisions based on that in your own “monolithic” plugin.

Given all that, I've been using it without many headaches, so it does can work.

## License

<a href="https://gitlab.com/interception/linux/plugins/xswitch/blob/master/LICENSE.md">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/License_icon-mit-2.svg/120px-License_icon-mit-2.svg.png" alt="MIT">
</a>

Copyright © 2021 Francisco Lopes da Silva

[interception-tools]: https://gitlab.com/interception/linux/tools
[`mux`]: https://gitlab.com/interception/linux/tools#mux
[x]: https://www.x.org
[cmake]: https://cmake.org
[interprocess]: https://www.boost.org/doc/libs/release/libs/interprocess
[xlib]: https://www.x.org/releases/current/doc/libX11/libX11/libX11.html
[xprop]: https://linux.die.net/man/1/xprop
[device-links]: https://gitlab.com/interception/linux/tools#device-links
[udevmon]: https://gitlab.com/interception/linux/tools/-/blob/master/udevmon.service
