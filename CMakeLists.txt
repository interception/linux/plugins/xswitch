cmake_minimum_required(VERSION 3.0)

project(xswitch)

find_package(Boost REQUIRED)
find_package(Threads REQUIRED)
find_package(X11 REQUIRED)

add_executable(xswitch xswitch.cpp)
target_include_directories(xswitch PRIVATE ${Boost_INCLUDE_DIRS} ${X11_INCLUDE_DIR})
target_compile_options(xswitch PRIVATE -Wall -Wextra -pedantic -std=c++11 -DBOOST_DATE_TIME_NO_LIB)
target_link_libraries(xswitch Threads::Threads X11::X11 X11::Xmu rt)

install(TARGETS xswitch RUNTIME DESTINATION bin)
